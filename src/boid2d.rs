use std::ops::*;
// use raylib::{*, core::Raylib};
use rand::prelude::*;

#[derive(PartialEq, Clone, Copy)]
pub struct Vec2 {
    x: f32,
    y: f32,
}

impl Add for Vec2 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}
impl Sub for Vec2 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl Mul<f32> for Vec2 {
    type Output = Vec2;

    fn mul(self, rhs: f32) -> Self::Output {
        Vec2 {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

impl Div<f32> for Vec2 {
    type Output = Vec2;

    fn div(self, rhs: f32) -> Self::Output {
        if rhs != 0.0 {
            Vec2 {
                x: self.x / rhs,
                y: self.y / rhs,
            }
        } else {
            self
        }
    }
}

impl Vec2 {
    pub fn sqrlen(&self) -> f32 {
        self.x * self.x + self.y * self.y
    }
}

#[derive(PartialEq, Clone, Copy)]
pub struct Boid2d {
    pos: Vec2,
    velocity: Vec2,
}

#[derive(PartialEq)]
pub struct Boid2dGroup {
    boids: Vec<Boid2d>,
}

impl<'a> IntoIterator for &'a Boid2dGroup {
    type Item = &'a Boid2d;
    type IntoIter = std::slice::Iter<'a, Boid2d>;

    fn into_iter(self) -> Self::IntoIter {
        self.boids.iter()
    }
}

impl Boid2dGroup {
    pub fn new(boid_nb: i32) -> Self {
        let mut boid_vec: Vec<Boid2d> = Vec::new();

        for _i in 0..boid_nb {
            let mut rng = rand::thread_rng();

            let x: f32 = rng.gen();
            let y: f32 = rng.gen();

            boid_vec.push(Boid2d {
                pos: Vec2 {
                    x: x * 95.0,
                    y: y * 95.0,
                },
                velocity: Vec2 { x: 0.0, y: 0.0 },
            });
        }
        Boid2dGroup { boids: boid_vec }
    }
    pub fn draw(&self) {
        todo!();
    }
    pub fn update(&mut self) {
        let mut buffer: Boid2dGroup = Boid2dGroup::new(self.boids.len() as i32);
        for i in 0..self.boids.len() {
            buffer.boids[i] = self.boids[i];
        }

        for i in 0..self.boids.len() {
            let v1 = self.cohesion(self.boids[i]);
            let v2 = self.separation(self.boids[i]);
            let v3 = self.allignement(self.boids[i]);

            buffer.boids[i].velocity = buffer.boids[i].velocity + v1 + v2 + v3;
            buffer.boids[i].pos = buffer.boids[i].pos + buffer.boids[i].velocity;
        }

        for i in 0..self.boids.len() {
            self.boids[i] = buffer.boids[i];
        }
    }

    fn cohesion(&self, boid: Boid2d) -> Vec2 {
        let mut pcj: Vec2 = Vec2 { x: 0.0, y: 0.0 };

        for b in self {
            if &boid != b {
                pcj.x += b.pos.x;
                pcj.y += b.pos.y;
            }
        }

        pcj = pcj / ((&self.boids.len() - 1) as f32);
        pcj = pcj - boid.pos;
        pcj / 100.0
    }

    fn separation(&self, boid: Boid2d) -> Vec2 {
        let mut c: Vec2 = Vec2 { x: 0.0, y: 0.0 };

        for b in self {
            if &boid != b && (b.pos - boid.pos).sqrlen() < 100.0 {
                c = c - b.pos + boid.pos;
            }
        }
        c
    }

    fn allignement(&self, boid: Boid2d) -> Vec2 {
        let mut pjv: Vec2 = Vec2 { x: 0.0, y: 0.0 };

        for b in self {
            if &boid != b {
                pjv = pjv + b.velocity;
            }
        }

        pjv = pjv / (&self.boids.len() - 1) as f32;
        pjv = pjv - boid.velocity;
        pjv / 8.0
    }
}


